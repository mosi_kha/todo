import falcon
import json
import uuid
import re
from falcon import errors

directory = 'todo.json'


class JsonFile:
    bass = dic = {
        "todo": [
        ]
    }

    @classmethod
    def write_file(cls, dic, path):  # input is dictionary object and path is a string
        with open(path, 'w') as f:
            json.dump(dic, f, indent=2, sort_keys=True)
        f.close()

    @classmethod
    def read_file(cls, path):
        try:
            with open(path, 'r') as f:
                return json.load(f)
        except FileNotFoundError:
            cls.write_file(cls.bass, directory)

    @classmethod
    def find_in_json(cls, path, key, value):
        """in this method , if key and value will be match,return the dic """
        data = JsonFile.read_file(path)
        new_json = []
        for i in range(len(data['todo'])):
            if data['todo'][i][key] == value:
                if key is 'id':
                    return data['todo'][i]  # because id is unique and when find  it we don't need keep processing
                else:
                    new_json.append(data['todo'][i])
        return new_json


class CompletedResource(object):

    def on_get(self, req, resp, completed):
        """check completed is a valid word """
        _true_list = ['true', 'True']
        _false_test = ['false', 'False']
        if completed in _true_list:
            completed = True
        elif completed in _false_test:
            completed = False
        else:
            raise errors.HTTPBadRequest('Invalid URI', {'Valid URI': [_true_list, _false_test]})

        resp.body = json.dumps(JsonFile.find_in_json(directory, 'completed', completed))


class SearchResource(object):
    def on_get(self, req, resp, query):
        """we use regex for search """
        data = JsonFile.read_file(directory)

        new_json = []
        for i in range(len(data['todo'])):
            if re.search(query, data['todo'][i]['text']):
                new_json.append(data['todo'][i])
        resp.body = json.dumps(new_json)


class IdentifyResource(object):
    """ I assure you make unique IDs and never save same IDs in your file and life :))"""

    def make_valid_json(self, dic):
        """check dic and make new valid json and return it"""
        if 'text' not in dic or type(dic['text']) is not str:  # text must be exist in req
            raise falcon.errors.HTTPBadRequest(
                'Invalid JSON',
                json.loads(
                    '{"id":"option (default use uuid.uuid4 and must be integer)",'
                    '"completed":"option (default false)","text":"is Necessary and type must be string"}')
            )
        else:
            new_json = {'text': dic['text']}
            if 'completed' in dic:
                if dic['completed'] in ['true', 'True'] or dic['completed'] is True:
                    new_json['completed'] = True
            else:
                new_json['completed'] = False

            if 'id' in dic:  # check if id exist or not , then if exist , check created before or not
                dic['id'] = self.make_id_int(dic['id'])
                exist = JsonFile.find_in_json(directory, 'id', dic['id'])
                if len(exist) == 0:
                    new_json['id'] = dic['id']
                else:
                    raise falcon.errors.HTTPConflict(
                        'id : {0} was created before'.format(dic['id'])
                    )
            else:
                new_json['id'] = int(uuid.uuid4())
            return new_json

    def make_id_int(self, idd):
        try:
            return int(idd)
        except ValueError:
            raise errors.HTTPBadRequest('id must be integer type')

    def on_get(self, req, resp, identify):
        """ '*' is a spacial character  to show all json dada in file"""
        if identify is '*':
            resp.body = json.dumps(JsonFile.read_file(directory))
            return

        identify = self.make_id_int(identify)
        exist_id = JsonFile.find_in_json(directory, 'id', identify)
        if len(exist_id) == 0:
            resp.status = falcon.HTTP_NO_CONTENT
        else:
            resp.status = falcon.HTTP_OK
            resp.body = json.dumps(exist_id)

    def on_post(self, req, resp, identify):
        """in this method , id in url is not our identification for create"""
        client_req = req.media

        data = JsonFile.read_file(directory)

        if type(client_req) is not list:  # make list for append
            client_req = [client_req]
        for item in range(len(client_req)):
            data['todo'].append(self.make_valid_json(client_req[item]))

        JsonFile.write_file(data, directory)
        resp.status = falcon.HTTP_CREATED

    def on_put(self, req, resp, identify):
        """this method update one dictionary and can not change ID """
        identify = self.make_id_int(identify)
        client_req = req.media
        if type(client_req) is not dict:
            raise falcon.errors.HTTPBadRequest('your request body must be a dictionary')

        exist_id = False

        my_data = JsonFile.read_file(directory)
        for i in range(len(my_data['todo'])):
            if identify == my_data['todo'][i]['id']:
                exist_id = my_data['todo'][i]
                dic = my_data['todo'][i]
                if 'completed' in client_req:
                    if client_req['completed'] in ['true', 'True'] or client_req['completed'] is True:
                        dic['completed'] = True
                    else:
                        dic['completed'] = False

                if 'text' in client_req:
                    if type(client_req['text']) is not str:
                        raise errors.HTTPBadRequest('Invalid text', 'text\' type must be string ')
                    else:
                        dic['text'] = client_req['text']

        JsonFile.write_file(my_data, directory)

        if exist_id is False:
            resp.status = falcon.HTTP_NO_CONTENT
        else:
            resp.status = falcon.HTTP_OK
            resp.body = json.dumps(exist_id)

    def on_delete(self, req, resp, identify):
        identify = self.make_id_int(identify)
        exist_id = False
        data = JsonFile.read_file(directory)
        for item in range(len(data['todo'])):
            if identify == data['todo'][item]['id']:
                data['todo'].pop(item)
                exist_id = True
                break  # if use return , we get of def

        JsonFile.write_file(data, directory)

        if exist_id is False:  # it's mean id is't available
            resp.status = falcon.HTTP_NO_CONTENT  # todo
            resp.body = json.dumps({"message": "{0} does not exist".format(identify)})
            return

        resp.status = falcon.HTTP_OK
        resp.body = json.dumps({'message': '{0} is deleted successful!'.format(identify)})


api = application = falcon.API()

api.add_route('/todos/{completed}', CompletedResource())  # allows methods : GET
api.add_route('/todo/{identify}', IdentifyResource())  # allows methods : GET POST PUT DELETE
api.add_route('/search/{query}', SearchResource())  # allows methods : GET
